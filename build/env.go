package build

import (
	"fmt"
	"log"
)

const (
	ModelDev  string = "dev"
	ModelProd string = "prod"
)

// Version 构建版本
var Version = "v0.0.0.beat"

// Model 构建模式 dev|prod
var Model = ModelDev

func IsProd() bool {
	return Model == ModelProd
}

func ShowBuildInfo() {
	log.Println(fmt.Sprintf("hookapi version:%s model:%s.", Version, Model))
}
