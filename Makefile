BINARY_NAME=hookapi
VERSION=v1.0.0.beat
# model: dev | prod
MODEL=prod


# -------------------- build script --------------------

BUILD_ARG=-ldflags="-s -w -X "nodemessage.com/hook-for-gitee/build.Version=${VERSION}" -X "nodemessage.com/hook-for-gitee/build.Model=${MODEL}""

.PHONY: all
all: win linux freebsd pkg

.PHONY: linux
linux:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0  go build  $(BUILD_ARG)  -o ./bin/linux/$(BINARY_NAME) ./main.go
	upx -9 ./bin/linux/$(BINARY_NAME)

.PHONY: win
win:
	GOOS=windows GOARCH=amd64 go build $(BUILD_ARG) -o ./bin/win/$(BINARY_NAME).exe ./main.go
	upx -9 ./bin/win/$(BINARY_NAME).exe

.PHONY: freebsd
freebsd:
	GOOS=freebsd GOARCH=amd64 CGO_ENABLED=0 go build  $(BUILD_ARG) -o ./bin/freebsd/$(BINARY_NAME) ./main.go

.PHONY: build
build: win linux freebsd

.PHONY: pkg
pkg:
	7z a ./bin/$(BINARY_NAME)-win.zip ./bin/win/$(BINARY_NAME).exe
	7z a ./bin/$(BINARY_NAME)-linux.zip ./bin/linux/$(BINARY_NAME)
	7z a ./bin/$(BINARY_NAME)-freebsd.zip ./bin/freebsd/$(BINARY_NAME)

.PHONY: install
install:
	go mod tidy