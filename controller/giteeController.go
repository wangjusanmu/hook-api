package controller

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"nodemessage.com/hook-for-gitee/service/impl"
	"nodemessage.com/hook-for-gitee/utils"
)

type GiteeController struct {
	Group *gin.RouterGroup
}

var service = impl.GiteeHookService{}

func (r *GiteeController) Get() {
	r.Group.POST("/:uuid", func(c *gin.Context) {
		utils.SignFun(c.Param("uuid"), func() {
			service.Handler(c)
		}, func() {
			log.Println(c.Param("uuid"), ",接口执行中忽略此次请求")
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status": "ok",
			})
		})
	})
}

func (r *GiteeController) Home() {
	r.Group.GET("/", func(c *gin.Context) {
		_, _ = c.Writer.WriteString("hook-api! setting :[POST] /:uuid")
	})
}
