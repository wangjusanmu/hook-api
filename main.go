package main

import (
	"nodemessage.com/hook-for-gitee/build"
	"nodemessage.com/hook-for-gitee/config"
)

func main() {
	build.ShowBuildInfo()
	config.Initialize()
}
