package config

import (
	"fmt"
	"log"
	"nodemessage.com/hook-for-gitee/resource"
	"nodemessage.com/hook-for-gitee/utils"
	"os"
	"path/filepath"
)

func Initialize() {
	fmt.Println("可访问配置说明: https://gitee.com/wangjusanmu/hook-api")
	releaseConfIni()
	readConf()
	server()
}

var confIniFileName = "hookapi.ini"

func releaseConfIni() {
	path := filepath.Join(utils.BinRunPath(), confIniFileName)
	_, err := os.Stat(path)
	if err == nil {
		return
	}
	if os.IsNotExist(err) {
		err := os.WriteFile(path, []byte(resource.ConfIni), 0666)
		if err != nil {
			log.Println("配置文件初始化错误,请在当前目录下手动创建", path, "文件.", err)
			os.Exit(1)
		}
	} else {
		log.Println("配置文件初始化错误,请在当前目录下手动创建", path, "文件.", err)
		os.Exit(1)
		return
	}
}
