package config

import (
	"fmt"
	"github.com/spf13/viper"
	"log"
	"nodemessage.com/hook-for-gitee/utils"
	"strings"
)

func readConf() {
	confSplit := strings.Split(confIniFileName, ".")
	viper.SetConfigName(confSplit[0])
	viper.SetConfigType(confSplit[1])
	viper.AddConfigPath(utils.BinRunPath())
	log.Println("配置文件路径:", utils.BinRunPath())
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(fmt.Errorf("Fatal error config file: %w \n", err))
	}
	//监听配置文件
	viper.WatchConfig()
}
