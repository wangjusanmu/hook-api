package config

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"nodemessage.com/hook-for-gitee/build"
	"nodemessage.com/hook-for-gitee/controller"
	"reflect"
)

/* hook服务接口 */
func server() {
	if build.IsProd() {
		gin.SetMode(gin.ReleaseMode)
	}
	r := gin.Default()

	/* 注册路由 */
	defaultGroup := r.Group("")
	RegisterController(&controller.GiteeController{Group: defaultGroup})
	/* 服务运行 */
	port := 8082
	if viper.InConfig("setting.port") {
		port = viper.GetInt("setting.port")
	}
	if err := r.Run(fmt.Sprintf(":%d", port)); err != nil {
		panic(err)
	}
}

func RegisterController(controller interface{}) {
	val := reflect.ValueOf(controller)
	numOfMethod := val.NumMethod()
	for i := 0; i < numOfMethod; i++ {
		val.Method(i).Call(nil)
	}
}
