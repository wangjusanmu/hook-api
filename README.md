<h1 align="center">Hook-Api (Alpha.1)</h1>
<div style="text-align: center">
<a href='https://gitee.com/wangjusanmu/hook-api/members'><img src='https://gitee.com/wangjusanmu/hook-api/badge/fork.svg?theme=dark' alt='fork'></img></a>
<a href='https://gitee.com/wangjusanmu/hook-api/stargazers'><img src='https://gitee.com/wangjusanmu/hook-api/badge/star.svg?theme=dark' alt='star'></img></a>
</div>
<p align="center"><strong>Hook-Api,Gitee hook api端实现.</strong></p>

[![王居三木/hook-api](https://gitee.com/wangjusanmu/hook-api/widgets/widget_card.svg?colors=ffffff,1e252b,323d47,455059,d7deea,99a0ae)](https://gitee.com/wangjusanmu/hook-api)

## 项目简介

Hook-Api,Gitee hook api端,CI/CD项目流程自动构建.

## 如何使用

1. 配置conf.ini文件.
    ```ini
   [setting]
   port = 8082

   [hook]
   hookKey = 'ping123'
   command = 'nohup java -jar test/framework.jar 2>&1 &'
   closeServer = kill -9 $(netstat -nlp | grep :8056 | awk '{print $7}' | awk -F"/" '{print $1}')
   branchs = dev,master
   protocol = ssh
    ```
2. 在仓库配置中设置GiteeHook基本信息.

   ![img.png](imgs/img.png)
3. 推送即可触发服务器钩子.

## 配置说明

| 参数             | 说明                                                                                               | 类型       |
|----------------|--------------------------------------------------------------------------------------------------|----------|
| [setting]      | 用于配置hook-api服务基本信息                                                                               | string   |
| port           | 服务端口,默认为8082                                                                                     | int      |
| [ <--path--> ] | 分组标识,Gitee推送的地址标识是服务访问的最后路径,如推送地址为 https://doc.nodemessage.com/hook ,则此配置为 'hook',该分组下的命令仅归该标识使用 | string   |
| hookKey        | 在Gitee Hook中配置的WebHook 密码                                                                        | string   |
| command        | 在推送代码后,服务端要执行的命令,执行路径在仓库的根目录下                                                                    | string   |
| closeServer    | 在接收到更新后,服务端更新服务前要停止服务的命令                                                                         | string   |
| branchs        | 接受出发钩子的推送分支                                                                                      | string[] |
| protocol       | git clone 时的地址类型,http,ssh                                                                        | string   |

### 当前阶段为开发预览版