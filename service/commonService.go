package service

import (
	"github.com/gin-gonic/gin"
)

type HookService interface {
	Handler(c *gin.Context)
}
